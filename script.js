"use strict"
const img = document.querySelectorAll('.image-to-show');
const firstImg = img[0];
const lastImg = img[img.length - 1];
const stopBtn = document.querySelector('.stop');
const startBtn = document.querySelector('.start');

const slide = () => {
    let currentImg= document.querySelector('.active');
    if (currentImg !== lastImg) {
        currentImg.classList.remove('active');
        currentImg.nextElementSibling.classList.add('active');
    } else {
        currentImg.classList.remove('active');
        firstImg.classList.add('active');
    }
};

let timer = setInterval(slide, 3000);

stopBtn.addEventListener('click', () => {
    clearInterval(timer);
    startBtn.disabled = false;
    stopBtn.disabled = true;

});

startBtn.addEventListener('click', () => {
    timer = setInterval(slide, 3000);
    startBtn.disabled = true;
    stopBtn.disabled = false;
});

